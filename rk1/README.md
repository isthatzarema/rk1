0. Создаём репозиторий через веб на GitLab
   * Видимость — Public (чтобы не аутентифицироваться при клонировании)
   * Видимость — Private:
     ```
     git config --global user.name "??User Name"
     git config --global user.email "??user-name@users.noreply.gitlab.com"
     ```


1. Клонируем репозиторий на локальный диск
   ```
   git clone https://gitlab.com/??user-name/??.git
   cd ??
   ```


2. Создаём проект в PhpStorm/IDEA
   * Тип проекта — NodeJS
   * Каталог проекта — основной каталог локального репозитория
   * Добавление и игнорирование файлов в проекте (.gitignore)
     ```git rm --cached -r .idea```
   * Эргономика:
     ```
     Maximize (F11)
     Recompile on changes (Settings)
     Выбор размера шрифтов (Setting, Ctrl + MouseWheel)
     Перетаскиваем все панели на право (Project, Commit, Structure, Favourites,
      npm, Git, Run, TODO, Problems, Terminal, Event Log)
     Hard wrap Markdown — 80 (Settings)
     ```


3. Commit + Push (если этот репозиторий первый, то будет запрос аутентификации
   на GitLab)
   * Рекомендуется делать небольшие и функционально законченные commit'ы со
     значимым комментарием (атомарные)
   * Remote-репозиториев может быть больше одного (хостить одновременно на
     GitLab и GitHub)


4. Настройка package.json
   * При создании проекта Node.js создается по-умолчанию IDEA
   * ```"main": "dist/index.js"```


5. Настройка tsconfig.json
   * Создавать через IDEA
   ```
   "target": "es6"
   "outDir": "dist"
   ```


6. Отладка кода
   * Для пошагового выполнению, в .ts файле обязательно надо поставить
     breakpoint
   * Иконка с жуком возле Run
   * В Navbar главный файл .js